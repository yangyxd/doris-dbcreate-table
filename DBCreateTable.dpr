program DBCreateTable;

uses
  System.StartUpCopy,
  FMX.Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Unit2 in 'Unit2.pas' {Frame2: TFrame},
  Unit3 in 'Unit3.pas' {Form3},
  Unit4 in 'Unit4.pas' {Frame4: TFrame},
  Unit5 in 'Unit5.pas' {Frame5: TFrame};

{$R *.res}
{$I 'buildConfig.inc'}

begin
  Application.Initialize;
  {$IFDEF SQLTool}
  Application.CreateForm(TForm3, Form3);
  {$ELSE}
  Application.CreateForm(TForm1, Form1);
  {$ENDIF}
  Application.Run;
end.
