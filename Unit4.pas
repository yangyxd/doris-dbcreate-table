unit Unit4;

interface

uses
  FMX.Platform,
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Menus, UI.Standard, UI.Edit, FMX.Controls.Presentation, UI.Base,
  FMX.ListBox;

type
  TFrame4 = class(TFrame)
    LinearLayout1: TLinearLayout;
    LinearLayout4: TLinearLayout;
    cbName: TCheckBox;
    tvHint: TEditView;
    LinearLayout3: TLinearLayout;
    edtHashKeys: TEditView;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    LinearLayout2: TLinearLayout;
    edtPrimaryKey: TEditView;
    edtPartitionKey: TEditView;
    TextView1: TTextView;
    tvFields: TTextView;
    TextView2: TTextView;
    edtLifecycle: TEditView;
    cbStoragePolicy: TComboBox;
    edtHotParNum: TEditView;
    cbMode: TComboBox;
    ckIndexAll: TCheckBox;
    procedure MenuItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CopyText(const Text: string);
  end;

implementation

{$R *.fmx}

{ TFrame4 }

procedure TFrame4.CopyText(const Text: string);
var
  FClipboardSvc: IFMXClipboardService;
begin
  if not TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, IInterface(FClipboardSvc)) then
    Exit;
  FClipboardSvc.SetClipboard(Text);
end;

procedure TFrame4.MenuItem1Click(Sender: TObject);
begin
  CopyText(cbName.Text);
end;

end.
