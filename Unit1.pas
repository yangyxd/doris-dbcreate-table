unit Unit1;

interface

uses
  Unit2, Unit4, Math,
  System.Generics.Collections,
  FMX.Clipboard,
  FMX.Platform, System.JSON, UI.Json,
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, UI.Edit,
  UI.Standard, UI.Base, FMX.Controls.Presentation, FMX.StdCtrls,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.MySQL, FireDAC.Phys.MySQLDef, FireDAC.FMXUI.Wait,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, UI.ListView, FMX.ListBox,
  UI.Dialog, UI.Toast, FMX.Menus, FMX.Edit, FMX.ComboEdit;

type
  TFieldItem = class
    Name: string;
    DataType: string;
    CharLen: Integer;
    Unsigned: Boolean;
    Comment: string;
    NumberPrecision: Integer;
    NumberScale: Integer;
    IsNotNull: Boolean;
    IsPriKey: Boolean;
    IsKey: Boolean;
    Index: Integer;
    DefaultValue: Variant;
  end;
  
  TItemData = class
    Name: string;
    Hint: string;
    Selected: Boolean;
    HashKeys: string;
    ModeKeys: string;

    // DorisDB
    Mode: Integer;
    HashBuckets: Integer;
    ReNum: Integer;  // 副本数
    GroupFun: string;  // 聚合函数设置：一个JSON字符串，内容为 {"字段名": "聚合函数名"}

    // ADB
    ParLifeCycle: Integer;
    PartitionValue: string;
    IsIndexAll: Boolean;
    IsBroadCast: Boolean;
    StoragePolicyType: Integer; // HOT, COLD, MIXED
    HotPartNum: Integer;

    Fields: TList<TFieldItem>;
    FieldMap: TDictionary<string, string>;
    DefaultHashKeys: string;
    DefaultModeKeys: string;

    constructor Create(); 
    destructor Destroy; override;

    procedure UpdateDefaultKeys(Owner: TForm; Mode: Integer); overload;
    procedure UpdateDefaultKeys(GlobalHashKeys, GlobalModeKeys: TStrings; Mode: Integer); overload;
  end;


type
  TForm1 = class;
  
  TListDataAdapter = class(TListAdapter<TItemData>)
  private
    FOwner: TForm1;
    procedure DoSelectChange(Sender: TObject);
    procedure DoModeChange(Sender: TObject);
    procedure DoIsIndexAllChange(Sender: TObject);
    procedure DoBroadCastChange(Sender: TObject);
    procedure DoStoragePolicyChange(Sender: TObject);
    procedure DoModeKeysChange(Sender: TObject);
    procedure DoHashKeysChange(Sender: TObject);
    procedure DoHashBucketsChange(Sender: TObject);
    procedure DoReNumChange(Sender: TObject);
    procedure DoParLifeCycleChange(Sender: TObject);
    procedure DoHotParNumChange(Sender: TObject);
    procedure DoShowFields(Sender: TObject);
    procedure DoHintChange(Sender: TObject);
    procedure DoGroupChange(Sender: TObject);
    procedure DoPartitionKeyChange(Sender: TObject);
  protected
    function GetItem(const Index: Integer): Pointer; override;
    function GetView(const Index: Integer; ConvertView: TViewBase; Parent: TViewGroup): TViewBase; override;
    function ItemDefaultHeight: Single; override;
  public
  end;

  TForm1 = class(TForm)
    LinearLayout1: TLinearLayout;
    TextView1: TTextView;
    TextView2: TTextView;
    EditView1: TEditView;
    TextView3: TTextView;
    EditView2: TEditView;
    TextView4: TTextView;
    EditView3: TEditView;
    TextView5: TTextView;
    EditView4: TEditView;
    Button1: TButton;
    FDConnection1: TFDConnection;
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    FDQuery1: TFDQuery;
    DataSource1: TDataSource;
    TextView6: TTextView;
    ListViewEx1: TListViewEx;
    Button2: TButton;
    cbSvrName: TComboBox;
    TextView7: TTextView;
    TextView8: TTextView;
    edtBuckets: TEditView;
    edtHashKeys: TEditView;
    TextView9: TTextView;
    edtModeKeys: TEditView;
    ToastManager1: TToastManager;
    DialogStyleManager1: TDialogStyleManager;
    FDQuery2: TFDQuery;
    Button4: TButton;
    cbNotFieldComment: TCheckBox;
    EditView5: TEditView;
    EditView6: TEditView;
    CheckBox1: TCheckBox;
    MainMenu1: TMainMenu;
    miFile: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    miEdit: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem1: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    TextView10: TTextView;
    cbType: TComboBox;
    MenuItem14: TMenuItem;
    lbReNum: TTextView;
    edtReNum: TEditView;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure edtHashKeysExit(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure edtHashKeysTextChange(Sender: TObject);
    procedure edtModeKeysTextChange(Sender: TObject);
    procedure edtHashKeysChange(Sender: TObject);
    procedure edtModeKeysChange(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure EditView5TextChange(Sender: TObject);
    procedure EditView5Change(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure MenuItem10Click(Sender: TObject);
    procedure MenuItem11Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure cbTypeChange(Sender: TObject);
    procedure MenuItem14Click(Sender: TObject);
  private
    { Private declarations }
    FMode: Integer;
    FDefaultValueChange: Boolean;
    FAdapter: TListDataAdapter;
    FItems: TList<TItemData>;
    FRawItems: TList<TItemData>;
    FDefaultModeKeys: string;
    FDefaultHashKeys: string;
    FDefaultIndexKeys: string;
    FDefaultHashBuckets, FDefaultLifecycle, FDefaultReNum: Integer;
    FCurDataBase: string;
    FConfig: TJSONObject;
    FLastSearchKey: string;
  protected
    function GetGlobalKeys(const Text: string): TStrings;
    function CheckKeys(const Text: string; Item: TItemData): string;
    procedure LoadConfig;
    procedure SaveConfig;
  public
    { Public declarations }
    procedure CopyText(const Text: string);
    procedure DoSearch(const Key: string);
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  Unit3;

type
  IIf<T> = class
  public
    class function Get(State: Boolean; TrueValue: T; FalseValue: T): T;
  end;

procedure TForm1.Button1Click(Sender: TObject);
var
  S: string;
  List: TStrings;
begin
  TFDPhysMySQLConnectionDefParams(FDConnection1.Params).Server := EditView1.Text;
  TFDPhysMySQLConnectionDefParams(FDConnection1.Params).Port := StrToIntDef(EditView2.Text, 3306);
  FDConnection1.Params.UserName := EditView3.Text.Trim;
  FDConnection1.Params.Password := EditView4.Text;
  try
    FDConnection1.Open();
    if FDConnection1.Connected then begin
      // 获取所有数据库列表
      S := '';
      if cbSvrName.Selected <> nil then
        S := cbSvrName.Selected.Text;
      if S = '' then
        S := FCurDataBase;
      cbSvrName.Items.Clear;
      FDQuery1.Open('SHOW DATABASES;');
      List := TStringList.Create;
      try
        if FDQuery1.Active then begin
          FDQuery1.First; 
          while not FDQuery1.Eof do begin
            List.Add(FDQuery1.FieldByName('Database').AsString);
            FDQuery1.Next;        
          end;
        end;
        cbSvrName.Items.AddStrings(List);
        if (S <> '') and (List.Count > 0) then begin
          cbSvrName.ItemIndex := List.IndexOf(S);
        end;
      finally
        FreeAndNil(List);
      end;
      Toast('连接成功');
    end;
  except on E: Exception do
    Toast(e.Message);
  end;
end;


type
  TWaitDlg = class(TProgressDialog);

procedure TForm1.Button2Click(Sender: TObject);

  procedure InitFields(Item: TItemData);
  var
    Field: TFieldItem;
    Map: TDictionary<string, TFieldItem>;
    Name: string;
  begin
    try
      Item.Fields.Clear;
      FDQuery2.Close;
      FDQuery2.Open('select * from information_schema.COLUMNS where table_name = ''' + item.Name + 
        ''' and TABLE_SCHEMA = ''' + FCurDataBase + ''';');
      Map := TDictionary<string, TFieldItem>.Create();
      try
        if FDQuery2.Active then begin
          FDQuery2.First;
          while not FDQuery2.Eof do begin
            Field := TFieldItem.Create;
            Field.Name := FDQuery2.FieldByName('COLUMN_NAME').AsString;
            Field.DataType := FDQuery2.FieldByName('DATA_TYPE').AsString.ToLower;
            Field.CharLen := FDQuery2.FieldByName('CHARACTER_MAXIMUM_LENGTH').AsInteger;
            Field.Unsigned := Pos(' unsigned', FDQuery2.FieldByName('COLUMN_TYPE').AsString.ToLower) > 0;
            Field.Comment := FDQuery2.FieldByName('COLUMN_COMMENT').AsString;
            Field.NumberPrecision := FDQuery2.FieldByName('NUMERIC_PRECISION').AsInteger;
            Field.NumberScale := FDQuery2.FieldByName('NUMERIC_SCALE').AsInteger;
            Field.IsNotNull := FDQuery2.FieldByName('IS_NULLABLE').AsString.ToUpper = 'NO';
            Field.IsPriKey := FDQuery2.FieldByName('COLUMN_KEY').AsString <> '';
            Field.Index := FDQuery2.FieldByName('ORDINAL_POSITION').AsInteger;
            Field.DefaultValue := FDQuery2.FieldByName('COLUMN_DEFAULT').Value;
            Item.Fields.Add(Field);
            Item.FieldMap.Add(Field.Name.ToLower, Field.Name);
            Map.Add(Field.Name, Field);
            FDQuery2.Next;
          end;
        end;
        FDQuery2.Close;
        FDQuery2.Open('select * from information_schema.statistics where table_name = ''' + item.Name +
        ''' and TABLE_SCHEMA = ''' + FCurDataBase + ''' and INDEX_NAME <> ''PRIMARY'';');
        if (FDQuery2.Active) then begin
          FDQuery2.First;
          while not FDQuery2.Eof do begin
            Name := FDQuery2.FieldByName('COLUMN_NAME').AsString;
            if Map.ContainsKey(Name) then begin
              Field := Map[Name];
              if not Field.IsKey then
                Field.IsKey := True;
            end;
            FDQuery2.Next;
          end;
        end;
      finally
        Map.Free;
      end;
    except on E: Exception do
    end;
  end;
  
var
  I: Integer;
  Item: TItemData;
  FWaitDlg: TWaitDlg;
  V: TJSONObject;
  ConfigData, Empty: TJSONObject;
  FGlobalHashKeys, FGlobalModeKeys: TStrings;
begin
  if not FDConnection1.Connected then begin
    Toast('服务器未连接');
    Exit;
  end;
  if cbSvrName.Selected = nil then begin
    Toast('请选择数据库');
    Exit;
  end;

  miFile.Enabled := False;
  miEdit.Enabled := False;
  FWaitDlg := TWaitDlg(TWaitDlg.Show(Self, '正在加载...              ', False));
  FGlobalHashKeys := GetGlobalKeys(FDefaultHashKeys);
  if FMode = 0 then
    FGlobalModeKeys := GetGlobalKeys(FDefaultModeKeys)
  else
    FGlobalModeKeys := GetGlobalKeys(FDefaultIndexKeys);
  Empty := TJSONObject.Create;
  try
    FLastSearchKey := '';
    EditView5.Text := '';
    FCurDataBase := cbSvrName.Selected.Text;
    ConfigData := FConfig.O['Datas'];
    if ConfigData <> nil then begin
      ConfigData := ConfigData.O[FCurDataBase.ToLower];
    end;
    FDQuery1.Close;
    FDQuery1.Open('SELECT * FROM information_schema.tables WHERE table_schema=''' + FCurDataBase + ''' ORDER BY table_name;');
    if FDQuery1.Active then begin
      FItems.Clear;
      FRawItems.Clear;
      FDQuery1.First;
      V := nil;
      while not FDQuery1.Eof do begin
        Item := TItemData.Create;
        Item.Name := FDQuery1.FieldByName('TABLE_NAME').AsString;
        if ConfigData <> nil then
          V := ConfigData.O[Item.Name];
        if V = nil then V := Empty;

        Item.Selected := IIf<Boolean>.Get(V <> Empty, V.B['Selected'], True);
        Item.Mode := IIf<Integer>.Get(V.Exist('Mode'), V.I['Mode'], 0);
        Item.ModeKeys := IIf<string>.Get(V.Exist('ModeKeys'), V.S['ModeKeys'], '');
        Item.HashKeys := IIf<string>.Get(V.Exist('HashKeys'), V.S['HashKeys'], '');
        Item.HashBuckets := IIf<Integer>.Get(V.Exist('HashBuckets'), V.I['HashBuckets'], 0);
        Item.ReNum := IIf<Integer>.Get(V.Exist('ReNum'), V.I['ReNum'], 0);
        Item.GroupFun := IIf<string>.Get(V.Exist('GroupFun'), V.S['GroupFun'], '');
        if V.Exist('Hint') then
          Item.Hint := V.S['Hint']
        else
          Item.Hint := FDQuery1.FieldByName('TABLE_COMMENT').AsString;

        Item.ParLifeCycle := IIf<Integer>.Get(V.Exist('ParLifeCycle'), V.I['ParLifeCycle'], 0);
        Item.IsIndexAll := IIf<Boolean>.Get(V <> Empty, V.S['IndexAll'] <> '0', True);
        Item.IsBroadCast := IIf<Boolean>.Get(V <> Empty, V.B['BroadCast'], False);
        Item.StoragePolicyType := IIf<Integer>.Get(V.Exist('StoragePolicy'), V.I['StoragePolicy'], 0);
        Item.HotPartNum := IIf<Integer>.Get(V.Exist('HotPartNum'), V.I['HotPartNum'], 0);
        Item.PartitionValue := IIf<string>.Get(V.Exist('ParValue'), V.S['ParValue'], '');

        FItems.Add(Item);
        FRawItems.Add(Item);
        FDQuery1.Next;                               
      end;
      for I := 0 to FRawItems.Count - 1 do begin
        FWaitDlg.SetMessage(Format('正在加载... %d/%d', [I, FRawItems.Count]));
        Application.ProcessMessages;  
        InitFields(FRawItems[I]);
        FRawItems[I].UpdateDefaultKeys(FGlobalHashKeys, FGlobalModeKeys, FMode);
      end;  
      edtHashKeysExit(nil);
    end;
  finally
    FWaitDlg.Dismiss;
    FreeAndNil(FGlobalModeKeys);
    FreeAndNil(FGlobalHashKeys);
    Empty.Free;
    miFile.Enabled := True;
    miEdit.Enabled := True;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
const
  S_MODEKEYS: TArray<string> = ['DUPLICATE KEY', 'AGGREGATE KEY', 'UNIQUE KEY'];
var
  I: Integer;
  Item: TItemData;
  SB: TStringBuilder;
  Fields: TList<TFieldItem>;

  procedure AddArray(List: TStrings; Value: TArray<string>; Map: TDictionary<string, string>);
  var
    I: Integer;
  begin
    for I := 0 to High(Value) do begin
      if Map.ContainsKey(Value[I].ToLower) then
        Continue;
      Map.Add(Value[I].ToLower, Value[I]);
      List.Add(Value[I]);
    end;
  end;

  procedure ExecItemDorisDB(Item: TItemData; SB: TStringBuilder);
  var
    Field: TFieldItem;
    Items: TStrings;
    Map: TDictionary<string, string>;
    FieldMap: TDictionary<string, TFieldItem>;
    I, M, N: Integer;
    key, dType, tmpStr: string;
    IsStr, IsDateTime: Boolean;
    JSON: TJSONObject;
  begin
    SB.Append('-- Table: ').Append(Item.Name).AppendLine;   
    try
      Items := TStringList.Create;
      Map := TDictionary<string, string>.Create();
      FieldMap := TDictionary<string, TFieldItem>.Create();
      try
        // 将字段排序， 将所有排序键放到前面
        AddArray(Items, IIf<string>.Get(item.ModeKeys = '', Item.DefaultModeKeys, Item.ModeKeys).Split([',']), Map);
        AddArray(Items, IIf<string>.Get(item.HashKeys = '', Item.DefaultHashKeys, Item.HashKeys).Split([',']), Map);
        for I := 0 to Item.Fields.Count - 1 do begin
          FieldMap.Add(Item.Fields[I].Name.ToLower, Item.Fields[I]);
          AddArray(Items, [Item.Fields[I].Name], Map);
        end;

        SB.Append('CREATE TABLE IF NOT EXISTS `').Append(Item.Name).Append('` (').AppendLine;
        for I := 0 to Items.Count - 1 do begin
          key := Items[I].ToLower;
          if not FieldMap.ContainsKey(key) then
            Continue;
          IsStr := False;
          IsDateTime := False;
          Field := FieldMap[key];
          SB.Append('  `').Append(Field.Name).Append('` ');

          dType := Field.DataType.Trim.ToLower;
          if dType = 'varchar' then begin
            SB.Append('VARCHAR(').Append(Field.CharLen * 3).Append(')');
            IsStr := True;
          end else if dType = 'char' then begin
            SB.Append('CHAR(').Append(Field.CharLen * 3).Append(')');
            IsStr := True;
          end else if (dType = 'text') or (dType = 'string') or (dType = 'tinytext') or (dType = 'mediumtext') or (dType = 'longtext') then begin
            SB.Append('STRING');
            IsStr := True;
          end else if dType = 'tinyint' then begin
            if Field.Unsigned then
              SB.Append('SMALLINT')
            else
              SB.Append('TINYINT');
          end else if dType = 'smallint' then begin
            if Field.Unsigned then
              SB.Append('INT')
            else
              SB.Append('SMALLINT');
          end else if (dType = 'int') or (dType = 'integer') then begin
            if Field.Unsigned then
              SB.Append('BIGINT')
            else
              SB.Append('INT');
          end else if (dType = 'bigint') then begin
            if Field.Unsigned then
              SB.Append('LARGEINT')
            else
              SB.Append('BIGINT');
          end else if (dType = 'float') then begin
            if Field.Unsigned then
              SB.Append('DOUBLE')
            else
              SB.Append('FLOAT');
          end else if (dType = 'double') then begin
            SB.Append('DOUBLE');
          end else if (dType = 'numeric') or (dType = 'decimal') then begin
            N := Field.NumberPrecision;
            if Field.Unsigned then N := N + 1;
            SB.Append('DECIMAL(').Append(Max(1, Min(N, 27))).Append(',');
            SB.Append(Max(1, Min(Field.NumberScale, 9))).Append(')');
          end else if (dType = 'date') or (dType = 'year') then begin
            SB.Append('DATE');
            IsDateTime := True;
          end else if (dType = 'time') or (dType = 'datetime') or (dType = 'timestamp') then begin
            SB.Append('DATETIME');
            IsDateTime := True;
          end else begin
            SB.Append(Field.DataType.ToUpper);
          end;

          // 聚合模式时，设置了聚合函数
          if (Item.Mode = 1) and (Item.GroupFun.Trim <> '') then begin
            try
              JSON := TJSONObject.Create;
              try
                JSOn.Parse(Item.GroupFun.ToLower);
                tmpStr := JSON.S[Field.Name.ToLower].ToUpper;
                if tmpStr <> '' then begin
                  SB.Append(' ').Append(JSON.S[Field.Name.ToLower].ToUpper);
                end;
              finally
                FreeAndNil(JSON);
              end;
            except
            end;
          end;

          if Field.IsNotNull then
            SB.Append(' NOT NULL');

          if not VarIsNull(Field.DefaultValue) then begin
            tmpStr := VarToStrDef(Field.DefaultValue, '');
            if IsStr then begin
              SB.Append(' DEFAULT ''');
              Sb.Append(tmpStr);
              SB.Append('''');
            end else if IsDateTime then begin
              //  and (tmpStr.ToUpper = 'CURRENT_TIMESTAMP')
            end else if tmpStr <> '' then begin
              SB.Append(' DEFAULT ''');
              Sb.Append(tmpStr);
              SB.Append('''');
            end;
          end;

          if (not cbNotFieldComment.IsChecked) and (Field.Comment.Trim <> '') then
            SB.Append(' COMMENT ''').Append(Field.Comment.Trim).Append('''');

          if I < Items.Count - 1 then
            SB.Append(',');

          SB.AppendLine;
        end;

        SB.Append(')').AppendLine;

        if Item.ModeKeys = '' then
          tmpStr := CheckKeys(Item.DefaultModeKeys, Item)
        else
          tmpStr := CheckKeys(Item.ModeKeys, Item);
        if tmpStr <> '' then begin
          SB.Append(S_MODEKEYS[Item.Mode]).Append('(');
          SB.Append(tmpStr);
          SB.Append(')').AppendLine;
        end;

        if (Item.Hint.Trim <> '') then
          SB.Append('COMMENT "').Append(Item.Hint.Trim).Append('"').AppendLine;

        SB.Append('DISTRIBUTED BY HASH(');
        if Item.HashKeys = '' then begin
          SB.Append(CheckKeys(Item.DefaultHashKeys, Item))
        end else begin
          SB.Append(CheckKeys(Item.HashKeys, Item))
        end;
        SB.Append(') ');

        SB.Append('BUCKETS ').Append(IIf<Integer>.Get(Item.HashBuckets > 0, Item.HashBuckets, FDefaultHashBuckets));
        M := IIf<Integer>.Get(Item.ReNum > 0, Item.ReNum, FDefaultReNum);
        if M <> 3 then begin
          SB.AppendLine.Append('PROPERTIES ( "replication_num" = "').Append(M).Append('" )');
        end;
        SB.Append(';').AppendLine;
      finally
        Items.Free;
        Map.Free;
        FieldMap.Free;
      end;
    except on E: Exception do
      SB.Append('-- Error: ' + e.Message).AppendLine;
    end;
    SB.AppendLine;
  end;

  procedure ExecItemADB(Item: TItemData; SB: TStringBuilder);
  var
    Field: TFieldItem;
    Items: TStrings;
    Map: TDictionary<string, string>;
    FieldMap: TDictionary<string, TFieldItem>;
    I, J, K, M, N: Integer;
    key, dType, tmpStr: string;
    IsStr, IsDateTime: Boolean;
    JSON: TJSONObject;
  begin
    SB.Append('-- Table: ').Append(Item.Name).AppendLine;   
    try
      Items := TStringList.Create;
      Map := TDictionary<string, string>.Create();
      FieldMap := TDictionary<string, TFieldItem>.Create();
      try
        // 将字段排序， 将所有排序键放到前面
        AddArray(Items, IIf<string>.Get(item.HashKeys = '', Item.DefaultHashKeys, Item.HashKeys).Split([',']), Map);
        if (Item.PartitionValue <> '') then begin
          J := Item.PartitionValue.IndexOf('(');
          K := Item.PartitionValue.IndexOf(',');
          if (J > 0) and (K > 0) and (K > J) then begin
            AddArray(Items, [Item.PartitionValue.Substring(J + 1, K - J - 1).Trim], Map);
          end else 
            AddArray(Items, [Item.PartitionValue], Map);            
        end;
        AddArray(Items, IIf<string>.Get(item.ModeKeys = '', Item.DefaultModeKeys, Item.ModeKeys).Split([',']), Map);
        for I := 0 to Item.Fields.Count - 1 do begin
          FieldMap.Add(Item.Fields[I].Name.ToLower, Item.Fields[I]);
          AddArray(Items, [Item.Fields[I].Name], Map);
        end;

        SB.Append('CREATE TABLE IF NOT EXISTS `').Append(Item.Name).Append('` (').AppendLine;
        for I := 0 to Items.Count - 1 do begin
          key := Items[I].ToLower;
          if not FieldMap.ContainsKey(key) then
            Continue;
          IsStr := False;
          IsDateTime := False;
          Field := FieldMap[key];
          SB.Append('  `').Append(Field.Name).Append('` ');

          dType := Field.DataType.Trim.ToLower;
          if dType = 'varchar' then begin
            SB.Append('VARCHAR');
            IsStr := True;
          end else if dType = 'char' then begin
            SB.Append('VARCHAR');
            IsStr := True;
          end else if (dType = 'text') or (dType = 'string') or (dType = 'tinytext') or (dType = 'mediumtext') or (dType = 'longtext') then begin
            SB.Append('VARCHAR');
            IsStr := True;
          end else if dType = 'tinyint' then begin
            if Field.Unsigned then
              SB.Append('SMALLINT')
            else
              SB.Append('TINYINT');
          end else if dType = 'smallint' then begin
            if Field.Unsigned then
              SB.Append('INT')
            else
              SB.Append('SMALLINT');
          end else if (dType = 'int') or (dType = 'integer') then begin
            if Field.Unsigned then
              SB.Append('BIGINT')
            else
              SB.Append('INT');
          end else if (dType = 'bigint') then begin
            if Field.Unsigned then
              SB.Append('DECIMAL(20,0)')
            else
              SB.Append('BIGINT');
          end else if (dType = 'float') then begin
            if Field.Unsigned then
              SB.Append('DOUBLE')
            else
              SB.Append('FLOAT');
          end else if (dType = 'double') then begin
            SB.Append('DOUBLE');
          end else if (dType = 'numeric') or (dType = 'decimal') then begin
            N := Field.NumberPrecision;
            if Field.Unsigned then N := N + 1;
            SB.Append('DECIMAL(').Append(Max(1, Min(N, 1000))).Append(',');
            SB.Append(Max(1, Min(Field.NumberScale, 1000))).Append(')');
          end else if (dType = 'date') or (dType = 'year') then begin
            SB.Append('DATE');
            IsDateTime := True;
          end else if (dType = 'time') then begin
            SB.Append('TIME');
            IsDateTime := True;
          end else if (dType = 'datetime') then begin
            SB.Append('DATETIME');
            IsDateTime := True;
          end else if (dType = 'timestamp') then begin
            SB.Append('TIMESTAMP');
            IsDateTime := True;
          end else begin
            SB.Append(Field.DataType.ToUpper);
          end;

          if Field.IsNotNull then
            SB.Append(' NOT NULL');

          if not VarIsNull(Field.DefaultValue) then begin
            tmpStr := VarToStrDef(Field.DefaultValue, '');
            if IsStr then begin
              SB.Append(' DEFAULT ''');
              Sb.Append(tmpStr);
              SB.Append('''');
            end else if IsDateTime then begin
              //  and (tmpStr.ToUpper = 'CURRENT_TIMESTAMP')
            end else if tmpStr <> '' then begin
              SB.Append(' DEFAULT ');
              Sb.Append(tmpStr);
            end;
          end;

          if (not cbNotFieldComment.IsChecked) and (Field.Comment.Trim <> '') then
            SB.Append(' COMMENT ''').Append(Field.Comment.Trim).Append('''');

          SB.Append(',');
          if (I < Items.Count - 1) then
            SB.AppendLine;
        end;
        if Item.ModeKeys = '' then
          tmpStr := CheckKeys(Item.DefaultModeKeys, Item)
        else
          tmpStr := CheckKeys(Item.ModeKeys, Item);
        if tmpStr <> '' then begin
          SB.AppendLine.Append('  PRIMARY KEY (');
          SB.Append(tmpStr);
          SB.Append(')').AppendLine;
        end else begin
          SB.Remove(SB.Length - 1, 1).AppendLine;
        end;
        SB.Append(')').AppendLine;

        if Item.IsBroadCast then begin
          SB.Append('DISTRIBUTED BY BROADCAST');
        end else begin
          if Item.HashKeys = '' then
            tmpStr := CheckKeys(Item.DefaultHashKeys, Item)
          else
            tmpStr := CheckKeys(Item.HashKeys, Item);
          SB.Append('DISTRIBUTE BY HASH(').Append(tmpStr).Append(')');
        end;

        M := 0;
        if Item.PartitionValue <> '' then begin
          M := IIf<Integer>.Get(Item.ParLifeCycle <= 0, FDefaultLifecycle, Item.ParLifeCycle);
          SB.AppendLine.Append('PARTITION BY VALUE(');
          SB.Append(Item.PartitionValue.Replace('DATE_FORMAT', 'DATE_FORMAT', [rfReplaceAll, rfIgnoreCase]));
          SB.Append(') LIFECYCLE ').Append(M);
        end;

        if Item.IsIndexAll then
          SB.AppendLine.Append('INDEX_ALL=''Y''');

        if (Item.PartitionValue <> '') and (Item.StoragePolicyType > 0) then begin
          SB.AppendLine.Append('STORAGE_POLICY=');
          if Item.StoragePolicyType = 2 then begin
            SB.Append('''MIXED'' ');
            SB.Append('HOT_PARTITION_COUNT ');
            SB.Append(IIf<Integer>.Get((Item.HotPartNum <= 0) or (Item.HotPartNum >= M), Min(10, M), Item.HotPartNum));
          end else
            SB.Append('''COLD''');
        end;

        if (Item.Hint.Trim <> '') then
          SB.AppendLine.Append('COMMENT "').Append(Item.Hint.Trim).Append('"');

        SB.Append(';').AppendLine;
      finally
        Items.Free;
        Map.Free;
        FieldMap.Free;
      end;
    except on E: Exception do
      SB.Append('-- Error: ' + e.Message).AppendLine;
    end;
    SB.AppendLine;
  end;

var
  AType: Integer;
begin
  if FItems.Count = 0 then begin
    Toast('表信息不存在');
    Exit;
  end;
  AType := cbType.ItemIndex;
  if AType < 0 then begin
    Toast('请选择DB类型');
    Exit;
  end;
  SB := TStringBuilder.Create;
  Fields := TList<TFieldItem>.Create;

  try
    SB.Append(Format('-- %s Create Table Script', [cbType.Selected.Text])).AppendLine;
    SB.Append('-- Date: ').Append(FormatDateTime('yyyy-MM-dd HH:mm:ss', Now())).AppendLine.AppendLine;
    for I := 0 to FItems.Count - 1 do begin
      Item := FItems[I];  
      if not Item.Selected then
        Continue;
      case AType of
        0: ExecItemDorisDB(Item, SB);
        1: ExecItemADB(Item, SB);
      end;
      Application.ProcessMessages;
    end;
    SB.Append('-- End Script').AppendLine;
  finally
    Fields.Free;
    CopyText(SB.ToString);
    SB.Free;
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  SaveConfig;
  Toast('保存成功');
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  I: Integer;
  Item: TItemData;
  SB: TStringBuilder;
begin
  if FItems.Count = 0 then begin
    Toast('表信息不存在');
    Exit;
  end;
  SB := TStringBuilder.Create;
  try
    SB.Append('-- DorisDB Drop Table Script').AppendLine;
    SB.Append('-- Date: ').Append(FormatDateTime('yyyy-MM-dd HH:mm:ss', Now())).AppendLine.AppendLine;
    for I := 0 to FItems.Count - 1 do begin
      Item := FItems[I];
      if not Item.Selected then
        Continue;
      SB.Append('DROP TABLE `').Append(Item.Name).Append('`;').AppendLine;
    end;
    SB.Append('-- End Script').AppendLine;
  finally
    CopyText(SB.ToString);
    SB.Free;
  end;
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  I, AType: Integer;
  Item: TItemData;
  SB: TStringBuilder;
  AHost, APort, AUser, APwd: string;
  ANeedInsert: Boolean;

  procedure AddArray(List: TStrings; Value: TArray<string>; Map: TDictionary<string, string>);
  var
    I: Integer;
  begin
    for I := 0 to High(Value) do begin
      if Map.ContainsKey(Value[I].ToLower) then
        Continue;
      Map.Add(Value[I].ToLower, Value[I]);
      List.Add(Value[I]);
    end;
  end;

  procedure ExecItemDorisDB(Item: TItemData; SB: TStringBuilder);
  var
    Field: TFieldItem;
    I: Integer;
    key, dType: string;
    Items: TStrings;
    Map: TDictionary<string, string>;
    FieldMap: TDictionary<string, TFieldItem>;
  begin
    SB.Append('-- Table: ').Append(Item.Name).Append('_ext').AppendLine;
    try
      Items := TStringList.Create;
      Map := TDictionary<string, string>.Create();
      FieldMap := TDictionary<string, TFieldItem>.Create();
      try
        // 将字段排序， 将所有排序键放到前面
        AddArray(Items, IIf<string>.Get(item.ModeKeys = '', Item.DefaultModeKeys, Item.ModeKeys).Split([',']), Map);
        AddArray(Items, IIf<string>.Get(item.HashKeys = '', Item.DefaultHashKeys, Item.HashKeys).Split([',']), Map);
        for I := 0 to Item.Fields.Count - 1 do begin
          FieldMap.Add(Item.Fields[I].Name.ToLower, Item.Fields[I]);
          AddArray(Items, [Item.Fields[I].Name], Map);
        end;

        // 将字段排序， 将所有排序键放到前面
        SB.Append('CREATE EXTERNAL TABLE IF NOT EXISTS `').Append(Item.Name).Append('_ext').Append('` (').AppendLine;
        for I := 0 to Items.Count - 1 do begin
          key := Items[I].ToLower;
          if not FieldMap.ContainsKey(key) then
            Continue;
          Field := FieldMap[key];
          SB.Append('  `').Append(Field.Name).Append('` ');

          dType := Field.DataType.Trim.ToLower;
          if dType = 'varchar' then begin
            SB.Append('VARCHAR(').Append(Field.CharLen).Append(')');
          end else if dType = 'char' then begin
            SB.Append('CHAR(').Append(Field.CharLen).Append(')');
          end else if (dType = 'text') or (dType = 'string') or (dType = 'tinytext') or (dType = 'mediumtext') or (dType = 'longtext') then begin
            SB.Append('STRING');
          end else if dType = 'tinyint' then begin
            if Field.Unsigned then
              SB.Append('SMALLINT')
            else
              SB.Append('TINYINT');
          end else if dType = 'smallint' then begin
            if Field.Unsigned then
              SB.Append('INT')
            else
              SB.Append('SMALLINT');
          end else if (dType = 'int') or (dType = 'integer') or (dType = 'mediumint') then begin
            if Field.Unsigned then
              SB.Append('BIGINT')
            else
              SB.Append('INT');
          end else if (dType = 'bigint') then begin
            if Field.Unsigned then
              SB.Append('LARGEINT')
            else
              SB.Append('BIGINT');
          end else if (dType = 'float') then begin
            if Field.Unsigned then
              SB.Append('DOUBLE')
            else
              SB.Append('FLOAT');
          end else if (dType = 'double') then begin
            SB.Append('DOUBLE');
          end else if (dType = 'numeric') or (dType = 'decimal') then begin
            SB.Append('DECIMAL(').Append(Max(1, Min(Field.NumberPrecision, 27))).Append(',');
            SB.Append(Max(1, Min(Field.NumberScale, 9))).Append(')');
          end else if (dType = 'date') or (dType = 'year') then begin
            SB.Append('DATE');
          end else if (dType = 'time') or (dType = 'datetime') or (dType = 'timestamp') then begin
            SB.Append('DATETIME');
          end else begin
            SB.Append(Field.DataType.ToUpper);
          end;

          if I < Items.Count - 1 then
            SB.Append(',');

          SB.AppendLine;
        end;

        SB.Append(')').AppendLine;
        SB.Append('ENGINE=mysql').AppendLine;
        SB.Append('PROPERTIES').AppendLine;
        SB.Append('(').AppendLine;
        SB.Append('  "host" = "').Append(AHost).Append('",').AppendLine;
        SB.Append('  "port" = "').Append(APort).Append('",').AppendLine;
        SB.Append('  "user" = "').Append(AUser).Append('",').AppendLine;
        SB.Append('  "password" = "').Append(APwd).Append('",').AppendLine;
        SB.Append('  "database" = "').Append(FCurDataBase).Append('",').AppendLine;
        SB.Append('  "table" = "').Append(Item.Name).Append('"').AppendLine;
        SB.Append(');').AppendLine;

        if ANeedInsert then begin
          SB.Append('INSERT INTO `').Append(Item.Name).Append('` SELECT * FROM `').Append(Item.Name).Append('_ext`;');
          SB.AppendLine;
        end;
      finally
        Items.Free;
        Map.Free;
        FieldMap.Free;
      end;
    except on E: Exception do
      SB.Append('-- Error: ' + e.Message).AppendLine;
    end;
    SB.AppendLine;
  end;

  procedure ExecItemADB(Item: TItemData; SB: TStringBuilder);
  var
    Field: TFieldItem;
    I, J, K: Integer;
    key, dType: string;
    Items: TStrings;
    Map: TDictionary<string, string>;
    FieldMap: TDictionary<string, TFieldItem>;
  begin
    SB.Append('-- Table: ').Append(Item.Name).Append('_ext').AppendLine;
    try
      Items := TStringList.Create;
      Map := TDictionary<string, string>.Create();
      FieldMap := TDictionary<string, TFieldItem>.Create();
      try
        // 将字段排序， 将所有排序键放到前面
        AddArray(Items, IIf<string>.Get(item.HashKeys = '', Item.DefaultHashKeys, Item.HashKeys).Split([',']), Map);
        if (Item.PartitionValue <> '') then begin
          J := Item.PartitionValue.IndexOf('(');
          K := Item.PartitionValue.IndexOf(',');
          if (J > 0) and (K > 0) and (K > J) then begin
            AddArray(Items, [Item.PartitionValue.Substring(J + 1, K - J - 1).Trim], Map);
          end else
            AddArray(Items, [Item.PartitionValue], Map);
        end;
        AddArray(Items, IIf<string>.Get(item.ModeKeys = '', Item.DefaultModeKeys, Item.ModeKeys).Split([',']), Map);
        for I := 0 to Item.Fields.Count - 1 do begin
          FieldMap.Add(Item.Fields[I].Name.ToLower, Item.Fields[I]);
          AddArray(Items, [Item.Fields[I].Name], Map);
        end;

        // 将字段排序， 将所有排序键放到前面
        SB.Append('CREATE TABLE IF NOT EXISTS `').Append(Item.Name).Append('_ext').Append('` (').AppendLine;
        for I := 0 to Items.Count - 1 do begin
          key := Items[I].ToLower;
          if not FieldMap.ContainsKey(key) then
            Continue;
          Field := FieldMap[key];
          SB.Append('  `').Append(Field.Name).Append('` ');

          dType := Field.DataType.Trim.ToLower;
          if dType = 'varchar' then begin
            SB.Append('VARCHAR(').Append(Field.CharLen).Append(')');
          end else if dType = 'char' then begin
            SB.Append('CHAR(').Append(Field.CharLen).Append(')');
          end else if (dType = 'text') or (dType = 'string') or (dType = 'tinytext') or (dType = 'mediumtext') or (dType = 'longtext') then begin
            SB.Append('VARCHAR');
          end else if (dType = 'binary') or (dType = 'blob') then begin
            SB.Append('BINARY');
          end else if dType = 'tinyint' then begin
            if Field.Unsigned then
              SB.Append('SMALLINT')
            else
              SB.Append('TINYINT');
          end else if dType = 'smallint' then begin
            if Field.Unsigned then
              SB.Append('INT')
            else
              SB.Append('SMALLINT');
          end else if (dType = 'int') or (dType = 'integer') or (dType = 'mediumint') then begin
            if Field.Unsigned then
              SB.Append('BIGINT')
            else
              SB.Append('INT');
          end else if (dType = 'bigint') then begin
            if Field.Unsigned then
              SB.Append('BIGINT')
            else
              SB.Append('BIGINT');
          end else if (dType = 'float') then begin
            if Field.Unsigned then
              SB.Append('DOUBLE')
            else
              SB.Append('FLOAT');
          end else if (dType = 'double') then begin
            SB.Append('DOUBLE');
          end else if (dType = 'numeric') or (dType = 'decimal') then begin
            SB.Append('DECIMAL(').Append(Max(1, Min(Field.NumberPrecision, 1000))).Append(',');
            SB.Append(Max(1, Min(Field.NumberScale, 1000))).Append(')');
          end else if (dType = 'date') or (dType = 'year') then begin
            SB.Append('DATE');
          end else if (dType = 'time') then begin
            SB.Append('TIME');
          end else if (dType = 'datetime') then begin
            SB.Append('DATETIME');
          end else if (dType = 'timestamp') then begin
            SB.Append('TIMESTAMP');
          end else begin
            SB.Append(Field.DataType.ToUpper);
          end;

          if I < Items.Count - 1 then
            SB.Append(',');

          SB.AppendLine;
        end;

        SB.Append(')').AppendLine;
        SB.Append('ENGINE=''mysql''').AppendLine;
        SB.Append('TABLE_PROPERTIES=''{').AppendLine;
        SB.Append('  "url":"jdbc:mysql://').Append(AHost).Append(':').Append(APort).Append('/').Append(FCurDataBase).Append('",').AppendLine;
        SB.Append('  "tablename":"').Append(Item.Name).Append('",').AppendLine;
        SB.Append('  "username":"').Append(AUser).Append('",').AppendLine;
        SB.Append('  "password":"').Append(APwd).Append('"').AppendLine;
        SB.Append('}'';').AppendLine;

        if ANeedInsert then begin
          SB.Append('REPLACE INTO `').Append(Item.Name).Append('` SELECT * FROM `').Append(Item.Name).Append('_ext`;');
          SB.AppendLine;
        end;
      finally
        Items.Free;
        Map.Free;
        FieldMap.Free;
      end;
    except on E: Exception do
      SB.Append('-- Error: ' + e.Message).AppendLine;
    end;
    SB.AppendLine;
  end;

begin
  if FItems.Count = 0 then begin
    Toast('表信息不存在');
    Exit;
  end;
  AType := cbType.ItemIndex;
  if AType < 0 then begin
    Toast('请选择DB类型');
    Exit;
  end;
  AHost := EditView6.Text.Trim;
  if AHost = '' then
    AHost := EditView1.Text.Trim;
  APort := EditView2.Text.Trim;
  AUser := EditView3.Text.Trim;
  APwd := EditView4.Text.Trim;
  ANeedInsert := CheckBox1.IsChecked;
  SB := TStringBuilder.Create;
  try
    SB.Append(Format('-- %s Create External Table Script', [cbType.Selected.Text])).AppendLine;
    SB.Append('-- Date: ').Append(FormatDateTime('yyyy-MM-dd HH:mm:ss', Now())).AppendLine.AppendLine;
    for I := 0 to FItems.Count - 1 do begin
      Item := FItems[I];
      if not Item.Selected then
        Continue;
      case AType of
        0: ExecItemDorisDB(Item, SB);
        1: ExecItemADB(Item, SB);
      end;
      if AType = 0 then
        ExecItemDorisDB(Item, SB);
      Application.ProcessMessages;
    end;
    SB.Append('-- End Script').AppendLine;
  finally
    CopyText(SB.ToString);
    SB.Free;
  end;
end;

procedure TForm1.cbTypeChange(Sender: TObject);
var
  M: Integer;
begin
  M := cbType.ItemIndex;
  FMode := cbType.ItemIndex;
  TextView7.Text := IIf<string>.Get(M = 0, 'BUCKETS:  ', 'LIFECYCLE: ');
  TextView9.Text := IIf<string>.Get(M = 0, '默认模型 Keys: ', '默认主键 Keys: ');
  edtModeKeys.TextHint := IIf<string>.Get(M = 0, '排序字段列表，以","分隔', '主键字段列表，以","分隔');
  edtBuckets.Text := IIf<string>.Get(M = 0, IntToStr(FDefaultHashBuckets), IntToStr(FDefaultLifecycle));
  lbReNum.Visible := FMode = 0;
  edtReNum.Visible := FMode = 0;
  FDefaultValueChange := True;
  edtHashKeysExit(edtHashKeys);
end;

function TForm1.CheckKeys(const Text: string; Item: TItemData): string;
var
  S: TStrings;
  I: Integer;
begin
  S := GetGlobalKeys(Text);
  for I := S.Count -1 downto 0 do begin
    if Item.FieldMap.ContainsKey(S[i].ToLower) then
      Continue;
    S.Delete(I);
  end;
  S.Delimiter := ',';
  Result := S.DelimitedText;
  S.Free;
end;

procedure TForm1.CopyText(const Text: string);
var
  FClipboardSvc: IFMXClipboardService;
begin
  if not TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, IInterface(FClipboardSvc)) then
    Exit;
  FClipboardSvc.SetClipboard(Text);
end;

procedure TForm1.DoSearch(const Key: string);
var
  I: Integer;
  S: string;
  Item: TItemData;
begin
  if FLastSearchKey = Key then Exit;
  FLastSearchKey := Key;
  if FRawItems.Count = 0 then
    Exit;
  FItems.Clear;
  S := Key.Trim.ToLower;
  for I := 0 to FRawItems.Count - 1 do begin
    Item := FRawItems[I];
    if (S = '') or (Item.Name.ToLower.IndexOf(S) >= 0) then
      FItems.Add(Item);
  end;
  FAdapter.NotifyDataChanged;
end;

procedure TForm1.EditView5Change(Sender: TObject);
begin
  DoSearch(TEditView(Sender).Text);
end;

procedure TForm1.EditView5TextChange(Sender: TObject);
begin
  DoSearch(TEditView(Sender).Text);
end;

procedure TForm1.edtHashKeysChange(Sender: TObject);
begin
  FDefaultValueChange := True;
end;

procedure TForm1.edtHashKeysExit(Sender: TObject);
var
  I: Integer;
  FKeys1, FKeys2: TStrings;
begin
  FDefaultHashKeys := edtHashKeys.Text.Trim;
  if cbType.ItemIndex = 0 then begin
    FDefaultModeKeys := edtModeKeys.Text.Trim;
    FDefaultHashBuckets := StrToIntDef(edtBuckets.Text, FDefaultHashBuckets);
    FDefaultReNum := StrToIntDef(edtReNum.Text, 3);
  end else begin
    FDefaultIndexKeys := edtModeKeys.Text.Trim;
    FDefaultLifecycle := StrToIntDef(edtBuckets.Text, FDefaultHashBuckets);
    FDefaultReNum := 0;
  end;
  if FDefaultValueChange then begin
    FKeys1 := GetGlobalKeys(FDefaultHashKeys);
    if cbType.ItemIndex = 0 then
      FKeys2 := GetGlobalKeys(FDefaultModeKeys)
    else
      FKeys2 := GetGlobalKeys(FDefaultIndexKeys);
    try
      for I := 0 to FRawItems.Count - 1 do
        FRawItems[I].UpdateDefaultKeys(FKeys1, FKeys2, FMode);
    finally
      FKeys1.Free;
      FKeys2.Free;
    end;
  end;
  FDefaultValueChange := False;
  if Assigned(FAdapter) then     
    FAdapter.NotifyDataChanged;
end;

procedure TForm1.edtHashKeysTextChange(Sender: TObject);
begin
  FDefaultValueChange := True;
end;

procedure TForm1.edtModeKeysChange(Sender: TObject);
begin
  FDefaultValueChange := True;
end;

procedure TForm1.edtModeKeysTextChange(Sender: TObject);
begin
  FDefaultValueChange := True;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SaveConfig;
  if FDConnection1.Connected then
    FDConnection1.Close;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FDefaultValueChange := False;
  Application.Title := Self.Caption;
  FItems := TList<TItemData>.Create();
  FRawItems := TList<TItemData>.Create();
  FAdapter := TListDataAdapter.Create(FItems);
  FAdapter.FOwner := Self;
  ListViewEx1.Adapter := FAdapter;
  FDefaultReNum := 3;
  FDefaultHashBuckets := 20;
  FDefaultLifecycle := 30;
  LoadConfig();
  FMode := cbType.ItemIndex;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FItems);
  FreeAndNil(FRawItems);
end;

function TForm1.GetGlobalKeys(const Text: string): TStrings;
var
  Items: TArray<string>;
  I: Integer;
begin
  Result := TStringList.Create;
  Items := Text.Split([',']);
  for I := 0 to Length(Items) - 1 do
    Result.Add(Items[I].Trim.ToLower);
end;

procedure TForm1.LoadConfig;
var
  S: TStrings;
begin
  if not Assigned(FConfig) then begin
    FConfig := TJSONObject.Create;
  end;
  try
    S := TStringList.Create;
    try
      S.LoadFromFile('config.json');
      FConfig.Parse(S.Text);
      EditView1.Text := FConfig.S['Host'];
      EditView2.Text := FConfig.S['Port'];
      EditView3.Text := FConfig.S['User'];
      EditView4.Text := FConfig.S['Pwd'];
      EditView6.Text := FConfig.S['LHost'];
      FCurDataBase := FConfig.S['DataBase'];
      FDefaultHashBuckets := StrToIntDef(FConfig.S['Buckets'], FDefaultHashBuckets);
      FDefaultLifecycle := StrToIntDef(FConfig.S['LifeCycle'], FDefaultLifecycle);
      edtHashKeys.Text := FConfig.S['HashKeys'];
      FDefaultModeKeys := FConfig.S['ModeKeys'];
      FDefaultIndexKeys := FConfig.S['IndexKeys'];
      cbType.ItemIndex := FConfig.I['Type'];
      edtBuckets.Text := Iif<Integer>.Get(cbType.ItemIndex = 0, FDefaultHashBuckets, FDefaultLifecycle).ToString;
      edtModeKeys.Text := Iif<string>.Get(cbType.ItemIndex = 0, FDefaultModeKeys, FDefaultIndexKeys);
    finally
      S.Free;
    end;
  except on E: Exception do
  end;
end;

procedure TForm1.MenuItem10Click(Sender: TObject);
var
  I: Integer;
  Item: TItemData;
  SB: TStringBuilder;
begin
  if FItems.Count = 0 then begin
    Toast('表信息不存在');
    Exit;
  end;
  SB := TStringBuilder.Create;
  try
    SB.Append('-- DorisDB Drop External Table Script').AppendLine;
    SB.Append('-- Date: ').Append(FormatDateTime('yyyy-MM-dd HH:mm:ss', Now())).AppendLine.AppendLine;
    for I := 0 to FItems.Count - 1 do begin
      Item := FItems[I];
      if not Item.Selected then
        Continue;
      SB.Append('DROP TABLE `').Append(Item.Name).Append('_ext').Append('`;').AppendLine;
    end;
    SB.Append('-- End Script').AppendLine;
  finally
    CopyText(SB.ToString);
    SB.Free;
  end;
end;

procedure TForm1.MenuItem11Click(Sender: TObject);
var
  I, AType: Integer;
  Item: TItemData;
  SB: TStringBuilder;
begin
  if FItems.Count = 0 then begin
    Toast('表信息不存在');
    Exit;
  end;
  AType := cbType.ItemIndex;
  if AType < 0 then begin
    Toast('请选择DB类型');
    Exit;
  end;
  SB := TStringBuilder.Create;
  try
    SB.Append('-- DorisDB Import External Table Script').AppendLine;
    SB.Append('-- Date: ').Append(FormatDateTime('yyyy-MM-dd HH:mm:ss', Now())).AppendLine.AppendLine;
    for I := 0 to FItems.Count - 1 do begin
      Item := FItems[I];
      if not Item.Selected then
        Continue;
      case AType of
        0:
          SB.Append('INSERT INTO `').Append(Item.Name).Append('` SELECT * FROM `').Append(Item.Name).Append('_ext').Append('`;').AppendLine;
        1:
          SB.Append('REPLACE INTO `').Append(Item.Name).Append('` SELECT * FROM `').Append(Item.Name).Append('_ext').Append('`;').AppendLine;
      end;
    end;
    SB.Append('-- End Script').AppendLine;
  finally
    CopyText(SB.ToString);
    SB.Free;
  end;
end;

procedure TForm1.MenuItem13Click(Sender: TObject);
var
  F: TForm3;
begin
  Self.Hide;
  F := TForm3.Create(Self);
  try
    F.ShowModal;
  finally
    F.Free;
    Self.Show;
  end;
end;

var
  FLastDBName: string = '';

procedure TForm1.MenuItem14Click(Sender: TObject);

  procedure AddArray(List: TStrings; Value: TArray<string>; Map: TDictionary<string, string>);
  var
    I: Integer;
  begin
    for I := 0 to High(Value) do begin
      if Map.ContainsKey(Value[I].ToLower) then
        Continue;
      Map.Add(Value[I].ToLower, Value[I]);
      List.Add(Value[I]);
    end;
  end;

var
  I, J, K, AType: Integer;
  Item: TItemData;
  SB: TStringBuilder;
  Items: TStrings;
  Field: TFieldItem;
  Map: TDictionary<string, string>;
  FieldMap: TDictionary<string, TFieldItem>;
  NewDBName, Key: string;
  FieldsStr: TStringBuilder;
begin
  if FItems.Count = 0 then begin
    Toast('表信息不存在');
    Exit;
  end;
  AType := cbType.ItemIndex;
  if AType < 0 then begin
    Toast('请选择DB类型');
    Exit;
  end;
  NewDBName := InputBox('源数据库名称', '', FLastDBName);
  if NewDBName = '' then begin
    Toast('请输入源数据库名称');
    Exit;
  end;
  FLastDBName := NewDBName;

  SB := TStringBuilder.Create;
  try
    SB.Append('-- DorisDB Import External Table Script').AppendLine;
    SB.Append('-- Date: ').Append(FormatDateTime('yyyy-MM-dd HH:mm:ss', Now())).AppendLine.AppendLine;
    for I := 0 to FItems.Count - 1 do begin
      Item := FItems[I];
      if not Item.Selected then
        Continue;
      Map := TDictionary<string, string>.Create();
      FieldMap := TDictionary<string, TFieldItem>.Create();
      Items := TStringList.Create;
      FieldsStr := TStringBuilder.Create;
      try
        // 将字段排序， 将所有排序键放到前面
        AddArray(Items, IIf<string>.Get(item.HashKeys = '', Item.DefaultHashKeys, Item.HashKeys).Split([',']), Map);
        if (Item.PartitionValue <> '') then begin
          J := Item.PartitionValue.IndexOf('(');
          K := Item.PartitionValue.IndexOf(',');
          if (J > 0) and (K > 0) and (K > J) then begin
            AddArray(Items, [Item.PartitionValue.Substring(J + 1, K - J - 1).Trim], Map);
          end else
            AddArray(Items, [Item.PartitionValue], Map);
        end;
        AddArray(Items, IIf<string>.Get(item.ModeKeys = '', Item.DefaultModeKeys, Item.ModeKeys).Split([',']), Map);
        for J := 0 to Item.Fields.Count - 1 do begin
          FieldMap.Add(Item.Fields[J].Name.ToLower, Item.Fields[J]);
          AddArray(Items, [Item.Fields[J].Name], Map);
        end;
        for J := 0 to Items.Count - 1 do begin
          key := Items[J].ToLower;
          if not FieldMap.ContainsKey(key) then
            Continue;
          Field := FieldMap[key];
          FieldsStr.Append('`').Append(Field.Name).Append('`,');
        end;
        FieldsStr.Remove(FieldsStr.Length - 1, 1);
        case AType of
          0:
            SB.Append('INSERT INTO `').Append(Item.Name).Append('` SELECT ').Append(FieldsStr.ToString).Append(' FROM `').Append(NewDBName).Append('`.`').Append(Item.Name).Append('`;').AppendLine;
          1:
            SB.Append('REPLACE INTO `').Append(Item.Name).Append('` SELECT ').Append(FieldsStr.ToString).Append(' FROM `').Append(NewDBName).Append('`.`').Append(Item.Name).Append('`;').AppendLine;
        end;
      finally
        Map.Free;
        Items.Free;
        FieldMap.Free;
        FieldsStr.Free;
      end;
    end;
    SB.Append('-- End Script').AppendLine;
  finally
    CopyText(SB.ToString);
    SB.Free;
  end;
end;

procedure TForm1.MenuItem5Click(Sender: TObject);
begin
  TDialogBuilder.Create(Self)
  .SetTitle('基本操作步骤')
  .SetMessage('1.配置MySQL服务器连接信息，建立连接<br>2.选择源数据库<br>3.获取表信息<br>4.在列表中修改表设置<br>5.点击编辑菜单中功能菜单复制相应的SQL脚本', True)
  .SetWidth(Min(Width * 0.85, 800))
  .SetNeutralButton('知道了', procedure (dlg: IDialog; w: Integer) begin
    dlg.Dismiss;
  end)
  .Show();
end;

procedure TForm1.SaveConfig;
var
  S: TStrings;
  Data, V: TJSONObject;
  Items: TJSONObject;
  I: Integer;
  Item: TItemData;
begin
  FConfig.S['Host'] := EditView1.Text;
  FConfig.S['Port'] := EditView2.Text;
  FConfig.S['User'] := EditView3.Text;
  FConfig.S['Pwd'] := EditView4.Text;
  FConfig.S['LHost'] := EditView6.Text;
  FConfig.S['DataBase'] := FCurDataBase;
  FConfig.S['Buckets'] := FDefaultHashBuckets.ToString;
  FConfig.S['LifeCycle'] := FDefaultLifeCycle.ToString;
  FConfig.S['HashKeys'] := edtHashKeys.Text;
  FConfig.S['ModeKeys'] := FDefaultModeKeys;
  FConfig.S['IndexKeys'] := FDefaultIndexKeys;
  FConfig.I['Type'] := cbType.ItemIndex;

  Data := FConfig.O['Datas'];
  if Data = nil then begin
    Data := FConfig.AddJsonObject('Datas');
  end;
  if (FCurDataBase <> '') and (FRawItems.Count > 0) then begin
    Data.RemovePair(FCurDataBase.ToLower);
    Items := Data.AddJsonObject(FCurDataBase.ToLower);
    for I := 0 to FRawItems.Count - 1 do begin
      Item := FRawItems[I];
      V := Items.AddJsonObject(Item.Name);
      V.B['Selected'] := Item.Selected;
      V.I['Mode'] := Item.Mode;
      V.S['ModeKeys'] := Item.ModeKeys;
      V.S['HashKeys'] := Item.HashKeys;
      V.I['HashBuckets'] := Item.HashBuckets;
      V.S['GroupFun'] := Item.GroupFun;
      V.I['ReNum'] := Item.ReNum;
      V.S['Hint'] := Item.Hint;

      V.I['ParLifeCycle'] := Item.ParLifeCycle;
      V.S['IndexAll'] := IIf<string>.Get(Item.IsIndexAll, '1', '0');
      V.B['BroadCast'] := Item.IsBroadCast;
      V.I['StoragePolicy'] := Item.StoragePolicyType;
      V.I['HotPartNum'] := Item.HotPartNum;
      V.S['ParValue'] := Item.PartitionValue;
    end;
  end;

  S := TStringList.Create;
  try
    S.Text := FConfig.ToJSON;
    S.SaveToFile('config.json');
  finally
    S.Free;
  end;
end;

{ TListDataAdapter }

procedure TListDataAdapter.DoBroadCastChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.IsBroadCast := TComboBox(Sender).ItemIndex = 1;
  TFrame4(TControl(Sender).TagObject).edtHashKeys.Enabled := not V.IsBroadCast;
  V.UpdateDefaultKeys(FOwner, FOwner.FMode);
  TFrame4(TControl(Sender).TagObject).edtPrimaryKey.Text := V.ModeKeys;
  TFrame4(TControl(Sender).TagObject).edtPrimaryKey.TextHint := V.DefaultModeKeys;
  TFrame4(TControl(Sender).TagObject).edtHashKeys.Text := V.HashKeys;
  TFrame4(TControl(Sender).TagObject).edtHashKeys.TextHint := '分布键: ' + V.DefaultHashKeys;
end;

procedure TListDataAdapter.DoGroupChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.GroupFun := TEditView(Sender).Text;
end;

procedure TListDataAdapter.DoHashBucketsChange(Sender: TObject);
var
  V: TItemData;
  S: string;
begin
  V := Items[TControl(Sender).Tag];
  S := TEditView(Sender).Text.Trim;
  if (s = '') or (S = '0') then
    V.HashBuckets := 0
  else
    V.HashBuckets := StrToIntDef(TEditView(Sender).Text.Trim, V.HashBuckets);
end;

procedure TListDataAdapter.DoHashKeysChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.HashKeys := TEditView(Sender).Text;
end;

procedure TListDataAdapter.DoHintChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.Hint := TEditView(Sender).Text;
end;

procedure TListDataAdapter.DoHotParNumChange(Sender: TObject);
var
  V: TItemData;
  S: string;
begin
  V := Items[TControl(Sender).Tag];
  S := TEditView(Sender).Text.Trim;
  if (s = '') or (S = '0') then
    V.HotPartNum := 0
  else
    V.HotPartNum := StrToIntDef(TEditView(Sender).Text.Trim, V.HotPartNum);
end;

procedure TListDataAdapter.DoIsIndexAllChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.IsIndexAll := TCheckBox(Sender).IsChecked;
end;

procedure TListDataAdapter.DoModeChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.Mode := TComboBox(Sender).ItemIndex;
end;

procedure TListDataAdapter.DoModeKeysChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.ModeKeys := TEditView(Sender).Text;
end;

procedure TListDataAdapter.DoParLifeCycleChange(Sender: TObject);
var
  V: TItemData;
  S: string;
begin
  V := Items[TControl(Sender).Tag];
  S := TEditView(Sender).Text.Trim;
  if (s = '') or (S = '0') then
    V.ParLifeCycle := 0
  else
    V.ParLifeCycle := StrToIntDef(TEditView(Sender).Text.Trim, V.ParLifeCycle);
end;

procedure TListDataAdapter.DoPartitionKeyChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.PartitionValue := TEditView(Sender).Text;
end;

procedure TListDataAdapter.DoReNumChange(Sender: TObject);
var
  V: TItemData;
  S: string;
begin
  V := Items[TControl(Sender).Tag];
  S := TEditView(Sender).Text.Trim;
  if (s = '') or (S = '0') then
    V.ReNum := 0
  else
    V.ReNum := StrToIntDef(TEditView(Sender).Text.Trim, V.ReNum);
end;

procedure TListDataAdapter.DoSelectChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.Selected := TCheckBox(Sender).IsChecked;
end;

function IsExist(const Text: string; const Value: string): Boolean;
var
  Items: TArray<string>;
  I: Integer;
  V: string;
begin
  V := Value.Trim.ToLower;
  Items := Text.ToLower.Split([',']);
  for I := 0 to High(Items) do begin
    if Items[i].Trim = v then begin
      Result := True;
      Exit;
    end;
  end;
  Result := False;
end;

procedure TListDataAdapter.DoShowFields(Sender: TObject);
var
  V: TItemData;
  Items, SS: TStrings;
  I: Integer;
  F: TFieldItem;
  S: string;
  Edt: TEditView;
  Selects: TArray<Boolean>;
  Map: TDictionary<string, TFieldItem>;
begin
  V := Self.Items[TControl(Sender).Tag];
  Items := TStringList.Create;
  Map := TDictionary<string, TFieldItem>.Create();
  SS := nil;
  Edt := nil;

  if FOwner.FMode = 0 then begin  
    if TFrame2(TControl(Sender).TagObject).edtHashKeys.IsFocused then
      Edt := TFrame2(TControl(Sender).TagObject).edtHashKeys
    else if TFrame2(TControl(Sender).TagObject).edtModeKeys.IsFocused then
      Edt := TFrame2(TControl(Sender).TagObject).edtModeKeys
  end else if FOwner.FMode = 1 then begin
    if TFrame4(TControl(Sender).TagObject).edtHashKeys.IsFocused then
      Edt := TFrame4(TControl(Sender).TagObject).edtHashKeys
    else if TFrame4(TControl(Sender).TagObject).edtPrimaryKey.IsFocused then
      Edt := TFrame4(TControl(Sender).TagObject).edtPrimaryKey
  end;

  SetLength(Selects, V.Fields.Count);
  if Edt <> nil then
    SS := FOwner.GetGlobalKeys(IIf<string>.Get(Edt.Text = '', Edt.TextHint, Edt.Text));

  for I := 0 to V.Fields.Count - 1 do begin
    F := V.Fields[I];
    Items.Add(Format('%s`%s`  %s%s', [
      IIf<string>.Get(F.IsPriKey, '** ', IIf<string>.Get(F.IsKey, '* ', '')),
      F.Name,
      F.DataType,
      IIf<string>.Get(F.DataType.ToLower = 'varchar',
        '('+F.CharLen.ToString+')',
        IIf<string>.Get((
          F.NumberPrecision > 0) and (F.NumberScale > 0),
          Format('(%d,%d)', [F.NumberPrecision, F.NumberScale]),
          ''
        )
      )
    ]));
    if (SS <> nil) and (SS.IndexOf(F.Name.ToLower) >= 0) then
      Selects[i] := True
    else
      Selects[I] := False;
  end;

  TDialogBuilder.Create(Form1)
    .SetTitle('选择字段')
    .SetMultiChoiceItems(Items, Selects, procedure (dlg: IDialog; idx: Integer; IsChecked: Boolean) begin
      Selects[Idx] := IsChecked;
    end)
    .SetWidth(Min(Form1.Width * 0.85, 480))
    .SetListItemDefaultHeight(30)
    .SetPositiveButton('取消', procedure (dlg: IDialog; Which: Integer) begin
      dlg.Dismiss;
    end)
    .SetNegativeButton('确定', procedure (dlg: IDialog; Which: Integer)
      var i: Integer; m: string;
      begin
        if Edt <> nil then begin
          S := '';
          for I := 0 to High(Selects) do begin
            if (not Selects[I]) then
              Continue;
            if (s = '') or s.EndsWith(',') then
              M := ''
            else
              M := ',';
            S := S + M + v.Fields[I].Name;
          end;
          Edt.Text := S;
          Edt.OnTextChange(Edt);
        end;
        dlg.Dismiss;
    end)
    .Show(procedure (Dialog: IDialog) begin
      Items.Free;
      FreeAndNil(SS);
      FreeAndNil(Map);
      if Edt <> nil then begin
        Edt.SetFocus;
        Edt.SelStart := Edt.Text.Length;
        Edt.SelLength := 0;
      end;
    end);
end;

procedure TListDataAdapter.DoStoragePolicyChange(Sender: TObject);
var
  V: TItemData;
begin
  V := Items[TControl(Sender).Tag];
  V.StoragePolicyType := TComboBox(Sender).ItemIndex;
  TEditView(TControl(Sender).TagObject).Enabled := V.StoragePolicyType = 2;
end;

function TListDataAdapter.GetItem(const Index: Integer): Pointer;
begin
  Result := Items[Index];
end;

function TListDataAdapter.GetView(const Index: Integer; ConvertView: TViewBase;
  Parent: TViewGroup): TViewBase;

  function GetDorisDBViewItem(Item: TItemData): TFrame2;
  begin
    if (ConvertView = nil) or (ConvertView.ClassName <> TFrame2.ClassName) then begin
      Result := TFrame2.Create(Parent);
      Result.Parent := Parent;
      Result.Width := Parent.Width;
      Result.Height := ItemDefaultHeight;
      Result.CanFocus := False;
    end else
      Result := TFrame2(ConvertView);
    Result.Tag := Index;

    Result.cbName.Tag := Index;
    Result.cbName.Text := Item.Name;
    Result.cbName.OnChange := DoSelectChange;
    Result.cbName.IsChecked := Item.Selected;

    Result.cbMode.Tag := Index;
    Result.cbMode.OnChange := DoModeChange;
    Result.cbMode.ItemIndex := Item.Mode;

    Result.edtModeKeys.Tag := Index;
    Result.edtModeKeys.OnTextChange := DoModeKeysChange;
    Result.edtModeKeys.OnChange := DoModeKeysChange;
    Result.edtModeKeys.Text := Item.ModeKeys;
    Result.edtModeKeys.TextHint := Item.DefaultModeKeys;

    Result.edtHashKeys.Tag := Index;
    Result.edtHashKeys.OnTextChange := DoHashKeysChange;
    Result.edtHashKeys.OnChange := DoHashKeysChange;
    Result.edtHashKeys.Text := Item.HashKeys;
    Result.edtHashKeys.TextHint := Item.DefaultHashKeys;

    Result.edtBuckets.Tag := Index;
    Result.edtBuckets.OnTextChange := DoHashBucketsChange;
    Result.edtBuckets.OnChange := DoHashBucketsChange;
    if Item.HashBuckets > 0 then
      Result.edtBuckets.Text := Item.HashBuckets.ToString
    else
      Result.edtBuckets.Text := '';
    Result.edtBuckets.TextHint := FOwner.FDefaultHashBuckets.ToString;

    Result.edtReNum.Tag := Index;
    Result.edtReNum.OnTextChange := DoReNumChange;
    Result.edtReNum.OnChange := DoReNumChange;
    if Item.ReNum > 0 then
      Result.edtReNum.Text := Item.ReNum.ToString
    else
      Result.edtReNum.Text := '';
    Result.edtReNum.TextHint := FOwner.FDefaultReNum.ToString;


    Result.tvHint.Tag := Index;
    Result.tvHint.OnTextChange := DoHintChange;
    Result.tvHint.OnChange := DoHintChange;
    Result.tvHint.Text := Item.Hint;

    Result.edtGroup.Tag := Index;
    Result.edtGroup.OnTextChange := DoGroupChange;
    Result.edtGroup.OnChange := DoGroupChange;
    Result.edtGroup.Text := Item.GroupFun;

    Result.tvFields.Tag := Index;
    Result.tvFields.OnClick := DoShowFields;
    Result.tvFields.TagObject := Result;
  end;

  function GetADBViewItem(Item: TItemData): TFrame4;
  begin
    if (ConvertView = nil) or (ConvertView.ClassName <> TFrame4.ClassName) then begin
      Result := TFrame4.Create(Parent);
      Result.Parent := Parent;
      Result.Width := Parent.Width;
      Result.Height := ItemDefaultHeight;
      Result.CanFocus := False;
    end else
      Result := TFrame4(ConvertView);
    Result.Tag := Index;

    Result.cbName.Tag := Index;
    Result.cbName.Text := Item.Name;
    Result.cbName.OnChange := DoSelectChange;
    Result.cbName.IsChecked := Item.Selected;

    Result.cbMode.Tag := Index;
    Result.cbMode.TagObject := Result;
    Result.cbMode.OnChange := DoBroadCastChange;
    Result.cbMode.ItemIndex := IIf<Integer>.Get(Item.IsBroadCast, 1, 0);

    Result.tvHint.Tag := Index;
    Result.tvHint.OnTextChange := DoHintChange;
    Result.tvHint.OnChange := DoHintChange;
    Result.tvHint.Text := Item.Hint;

    Result.edtHashKeys.Tag := Index;
    Result.edtHashKeys.OnTextChange := DoHashKeysChange;
    Result.edtHashKeys.OnChange := DoHashKeysChange;
    if Item.IsBroadCast then begin
      Result.edtHashKeys.Text := '';
      Result.edtHashKeys.TextHint := '分布键字段列表，以","分隔 (BROADCAST维度表不需指定分布键)';
      Result.edtHashKeys.Enabled := False;
    end else begin
      Result.edtHashKeys.Text := Item.HashKeys;
      Result.edtHashKeys.TextHint := '分布键: ' + Item.DefaultHashKeys;
      Result.edtHashKeys.Enabled := True;
    end;

    Result.edtPrimaryKey.Tag := Index;
    Result.edtPrimaryKey.OnTextChange := DoModeKeysChange;
    Result.edtPrimaryKey.OnChange := DoModeKeysChange;
    Result.edtPrimaryKey.Text := Item.ModeKeys;
    Result.edtPrimaryKey.TextHint := Item.DefaultModeKeys;

    Result.edtLifecycle.Tag := Index;
    Result.edtLifecycle.OnTextChange := DoParLifeCycleChange;
    Result.edtLifecycle.OnChange := DoParLifeCycleChange;
    if Item.ParLifeCycle > 0 then
      Result.edtLifecycle.Text := Item.ParLifeCycle.ToString
    else
      Result.edtLifecycle.Text := '';
    Result.edtLifecycle.TextHint := FOwner.FDefaultLifecycle.ToString;

    Result.edtPartitionKey.Tag := Index;
    Result.edtPartitionKey.OnTextChange := DoPartitionKeyChange;
    Result.edtPartitionKey.OnChange := DoPartitionKeyChange;
    Result.edtPartitionKey.Text := Item.PartitionValue;

    Result.cbStoragePolicy.Tag := Index;
    Result.cbStoragePolicy.TagObject := Result.edtHotParNum;
    Result.cbStoragePolicy.OnChange := DoStoragePolicyChange;
    Result.cbStoragePolicy.ItemIndex := Item.StoragePolicyType;

    Result.edtHotParNum.Tag := Index;
    Result.edtHotParNum.OnTextChange := DoHotParNumChange;
    Result.edtHotParNum.OnChange := DoHotParNumChange;
    if (Item.HotPartNum > 0) and (Item.StoragePolicyType = 2) then
      Result.edtHotParNum.Text := Item.HotPartNum.ToString
    else
      Result.edtHotParNum.Text := '';
    Result.edtHotParNum.Enabled := Item.StoragePolicyType = 2;

    Result.ckIndexAll.Tag := Index;
    Result.ckIndexAll.OnChange := DoIsIndexAllChange;
    Result.ckIndexAll.IsChecked := Item.IsIndexAll;

    Result.tvFields.Tag := Index;
    Result.tvFields.OnClick := DoShowFields;
    Result.tvFields.TagObject := Result;
  end;

var
  Item: TItemData;
begin
  Item := Items[Index];
  if Self.FOwner.FMode = 0 then
    Result := TViewBase(GetDorisDBViewItem(Item))
  else
    Result := TViewBase(GetADBViewItem(Item));
end;

function TListDataAdapter.ItemDefaultHeight: Single;
begin
  Result := 125;
end;

{ TItemData }

constructor TItemData.Create;
begin
  Fields := TList<TFieldItem>.Create;
  FieldMap := TDictionary<string, string>.Create();
end;

destructor TItemData.Destroy;
begin
  FreeAndNil(Fields);
  FreeAndNil(FieldMap);
  inherited;
end;

procedure TItemData.UpdateDefaultKeys(Owner: TForm; Mode: Integer);
var
  FGlobalHashKeys, FGlobalModeKeys: TStrings;
begin
  FGlobalHashKeys := TForm1(Owner).GetGlobalKeys(TForm1(Owner).FDefaultHashKeys);
  if Mode = 0 then
    FGlobalModeKeys := TForm1(Owner).GetGlobalKeys(TForm1(Owner).FDefaultModeKeys)
  else
    FGlobalModeKeys := TForm1(Owner).GetGlobalKeys(TForm1(Owner).FDefaultIndexKeys);
  try
    UpdateDefaultKeys(FGlobalHashKeys, FGlobalModeKeys, Mode);
  finally
    FreeAndNil(FGlobalHashKeys);
    FreeAndNil(FGlobalModeKeys);
  end;
end;

procedure TItemData.UpdateDefaultKeys(GlobalHashKeys, GlobalModeKeys: TStrings; Mode: Integer);
var
  List: TStrings;
  I: Integer;
  S: string;
begin
  List := TStringList.Create;
  List.Delimiter := ',';
  try
    // Hash Keys
    for I := 0 to Fields.Count - 1 do begin
      if Fields[i].IsPriKey then
        List.Add(Fields[i].Name);
    end;
    for I := 0 to GlobalHashKeys.Count - 1 do begin
      if FieldMap.ContainsKey(GlobalHashKeys[I]) then begin
        S := FieldMap[GlobalHashKeys[I]];
        if List.IndexOf(S) < 0 then
          List.Add(S);
      end;
    end;
    DefaultHashKeys := List.DelimitedText;
    List.Clear;

    // Mode or Index Keys
    if Mode = 1 then begin
      if not IsBroadCast then begin
        for I := 0 to GlobalHashKeys.Count - 1 do begin
          if FieldMap.ContainsKey(GlobalHashKeys[I]) then begin
            S := FieldMap[GlobalHashKeys[I]];
            if List.IndexOf(S) < 0 then
              List.Add(S);
          end;
        end;
      end;
      for I := 0 to Fields.Count - 1 do begin
        if Fields[i].IsPriKey and (List.IndexOf(Fields[i].Name) < 0) then
          List.Add(Fields[i].Name);
      end;
      for I := 0 to Fields.Count - 1 do begin
        if (Fields[i].IsKey) and (List.IndexOf(Fields[i].Name) < 0) then
          List.Add(Fields[i].Name);
      end;
    end else begin
      for I := 0 to Fields.Count - 1 do begin
        if (Fields[i].IsPriKey or Fields[i].IsKey) and (List.IndexOf(Fields[i].Name) < 0) then
          List.Add(Fields[i].Name);
      end;
    end;

    for I := 0 to GlobalModeKeys.Count - 1 do begin
      if FieldMap.ContainsKey(GlobalModeKeys[I]) then begin
        S := FieldMap[GlobalModeKeys[I]];
        if List.IndexOf(S) < 0 then
          List.Add(S);
      end;
    end;
    DefaultModeKeys := List.DelimitedText;
  finally
    List.Free;
  end;
end;

{ IIf<T> }

class function IIf<T>.Get(State: Boolean; TrueValue, FalseValue: T): T;
begin
  if State then
    Result := TrueValue
  else
    Result := FalseValue;
end;

end.
