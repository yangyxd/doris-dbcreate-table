unit Unit2;

interface

uses
  FMX.Platform,
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  UI.Base, FMX.Controls.Presentation, UI.Edit, UI.Standard, FMX.ListBox,
  FMX.Menus;

type
  TFrame2 = class(TFrame)
    LinearLayout1: TLinearLayout;
    LinearLayout2: TLinearLayout;
    TextView1: TTextView;
    edtModeKeys: TEditView;
    TextView2: TTextView;
    LinearLayout3: TLinearLayout;
    TextView3: TTextView;
    edtHashKeys: TEditView;
    TextView4: TTextView;
    edtBuckets: TEditView;
    cbMode: TComboBox;
    LinearLayout4: TLinearLayout;
    cbName: TCheckBox;
    tvHint: TEditView;
    tvFields: TTextView;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    edtGroup: TEditView;
    TextView5: TTextView;
    TextView6: TTextView;
    edtReNum: TEditView;
    procedure MenuItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CopyText(const Text: string);
  end;

implementation

{$R *.fmx}

procedure TFrame2.CopyText(const Text: string);
var
  FClipboardSvc: IFMXClipboardService;
begin
  if not TPlatformServices.Current.SupportsPlatformService(IFMXClipboardService, IInterface(FClipboardSvc)) then
    Exit;
  FClipboardSvc.SetClipboard(Text);
end;

procedure TFrame2.MenuItem1Click(Sender: TObject);
begin
  CopyText(cbName.Text);
end;

end.
